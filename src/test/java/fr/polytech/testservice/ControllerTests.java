package fr.polytech.testservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import fr.polytech.testservice.controllers.TestsController;
import fr.polytech.testservice.services.TestService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.NoSuchElementException;
import java.util.Optional;


import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(TestsController.class)
class ControllerTests {

	@Autowired
	private MockMvc mockMvc;


	@MockBean
	private TestService testService;

	private String asJsonString(Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		}catch(IOException e){
			throw new RuntimeException(e);
		}
	}

	@Test
	@DisplayName("GET /test/1 - Found")
	public void shouldReturnTestEntity() throws Exception {
		fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(1, new Timestamp(1000000), "PCR", true, "a34kAo90");
		doReturn(Optional.of(mockTest)).when(testService).get(1L);
		mockMvc.perform(get("/tests/1", 1))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id_test", is(1)))
				.andExpect(jsonPath("$.is_negative", is(true)))
				.andExpect(jsonPath("$.nom", is("PCR")))
				.andExpect(jsonPath("$.id_user", is("a34kAo90")));

	}


	@Test
	@DisplayName("GET /test/1 - Not Found")
	public void shouldNotReturnTestEntity() throws Exception {
		doReturn(Optional.empty()).when(testService).get(1L);

		mockMvc.perform(get("/tests/1", 1))
				.andExpect(status().isNotFound());
	}

	@Test
	@DisplayName("POST /tests - Success")
	public void shouldCreateEntity() throws Exception {
		fr.polytech.testservice.models.Test postTest = new fr.polytech.testservice.models.Test(new Timestamp(1000), "PCR", true, "ieu6azZA");
		fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(1, new Timestamp(1000), "PCR", true, "ieu6azZA");
		doReturn(mockTest).when(testService).create(any());

		mockMvc.perform(post("/tests")
				.contentType(MediaType.APPLICATION_JSON)
				.content(asJsonString(postTest)))
				// Response type and content type
				.andExpect(status().isCreated())

				// Validate the headers
				.andExpect(header().string(HttpHeaders.LOCATION, "/tests/1"));

				/* Uncomment if the entity is returned with a post method
				// Returned fields
				.andExpect(jsonPath("$.id_test", is(1)))
				.andExpect(jsonPath("$.nom", is("PCR")))
				.andExpect(jsonPath("$.is_negative", is(true)))
				.andExpect(jsonPath("$.id_user", is("ieu6azZA")));
				*/
	}

	@Test
	@DisplayName("DELETE /tests/1 - Success")
	public void shouldDeleteEntity() throws Exception {
		doNothing().when(testService).delete(any());
		mockMvc.perform(delete("/tests/1", 1))
				.andExpect(status().isOk());
	}

	@Test
	@DisplayName("DELETE tests/1 - Not found")
	public void shouldNotFindEntityOnDelete() throws Exception {
		doThrow(NoSuchElementException.class).when(testService).delete(any());
		mockMvc.perform(delete("/tests/{id}", 1))
				.andExpect(status().isNotFound());
	}

}
