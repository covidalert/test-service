package fr.polytech.testservice;

import fr.polytech.testservice.repositories.TestRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.test.context.junit4.SpringRunner;


import java.sql.Timestamp;
import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RepositoryTests {
    @Autowired
    private TestEntityManager entityManager;
    @Autowired
    private TestRepository repository;

    @Test
    @DisplayName("FindById - Found")
    void testFindByIdFound() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        Optional<fr.polytech.testservice.models.Test> foundTest = repository.findById(id);

        // Assertions
        Assertions.assertTrue(foundTest.isPresent(), "Test was not found");
        Assertions.assertSame(foundTest.get(), mockTest,  "Test found was not the same");
    }

    @Test
    @DisplayName("FindById - Not found")
    void testFindByIdNotFound() {
        // Data persists through every tests, so to ensure that a data with the provided id is not found,
        // we look for a data that has just been removed
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        entityManager.remove(mockTest);

        Optional<fr.polytech.testservice.models.Test> foundTest = repository.findById(id);

        // Assertions
        Assertions.assertFalse(foundTest.isPresent(), "Test was found");
    }

    @Test
    @DisplayName("SaveAndFlush - Success")
    public void testSaveAndFlushSuccess() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        fr.polytech.testservice.models.Test createdTest = repository.saveAndFlush(mockTest);

        // Assertions
        Assertions.assertSame(createdTest, mockTest,"Test was not the same");
        entityManager.remove(mockTest);
    }

    @Test
    @DisplayName("ExistsById - Found")
    public void existsByIdFound() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        Long id = (Long) entityManager.persistAndGetId(mockTest);

        boolean isFound = repository.existsById(id);

        // Assertions
        Assertions.assertTrue(isFound, "Test was not found");
    }

    @Test
    @DisplayName("ExistsById - Not found")
    public void existsByIdNotFound() {
        // Same than FindById not found
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        entityManager.remove(mockTest);

        boolean isFound = repository.existsById(id);

        // Assertions
        Assertions.assertFalse(isFound, "Test was not found");
    }

    @Test
    @DisplayName("ExistsById - Provided id null")
    public void existsByIdThrowException() {
        // Assertions
        Assertions.assertThrowsExactly(InvalidDataAccessApiUsageException.class, () -> repository.existsById(null));
    }

    @Test
    @DisplayName("DeleteById - Success")
    public void deleteByIdSuccess() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        repository.deleteById(id);
    }

    @Test
    @DisplayName("DeleteById - Not found")
    public void deleteByIdNotFound() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000),"PCR", true, "azert");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        entityManager.remove(mockTest);

        // Assertions
        Assertions.assertThrowsExactly(EmptyResultDataAccessException.class, () -> repository.deleteById(id));
    }
}
