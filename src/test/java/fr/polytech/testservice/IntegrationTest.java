package fr.polytech.testservice;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.sql.Timestamp;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@Transactional
@AutoConfigureTestEntityManager
@AutoConfigureTestDatabase
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class IntegrationTest {
    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private MockMvc mockMvc;

    private String asJsonString(Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    @DisplayName("GET /tests/{id} - Found")
    public void getFound() throws Exception {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000000), "PCR", true, "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        mockMvc.perform(get("/tests/{id}", id))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id_test", is(id.intValue())))
                .andExpect(jsonPath("$.nom", is("PCR")))
                .andExpect(jsonPath("$.is_negative", is(true)))
                .andExpect(jsonPath("$.id_user", is("a34kAo90")));
    }

    @Test
    @DisplayName("GET /tests/{id} - Not Found")
    public void getNotFound() throws Exception {
        // Add then remove the entry in the db, to ensure that the id is unavailable
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000000), "PCR", true, "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        entityManager.remove(mockTest);

        mockMvc.perform(get("/tests/{id}", id))
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("POST /tests - Success")
    public void postSuccess() throws Exception {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000000), "PCR", true, "a34kAo90");

        mockMvc.perform(post("/tests")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(mockTest)))
                .andExpect(status().isCreated());
    }

    @Test
    @DisplayName("DELETE /tests/{id} - Success")
    public void deleteSuccess() throws Exception {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000000), "PCR", true, "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockTest);

        mockMvc.perform(delete("/tests/{id}",id))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("DELETE /tests/{id} - Not found")
    public void deleteNotFound() throws Exception {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(new Timestamp(1000000), "PCR", true, "a34kAo90");
        Long id = (Long) entityManager.persistAndGetId(mockTest);
        entityManager.remove(mockTest);

        mockMvc.perform(delete("/tests/{id}",id))
                .andExpect(status().isNotFound());
    }
}
