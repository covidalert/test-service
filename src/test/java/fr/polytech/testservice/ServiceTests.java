package fr.polytech.testservice;

import fr.polytech.testservice.repositories.TestRepository;
import fr.polytech.testservice.services.TestService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.sql.Timestamp;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@WebMvcTest(TestService.class)
public class ServiceTests {
    @Autowired
    private TestService testService;

    @MockBean
    private TestRepository testRepository;

    @Test
    @DisplayName("get - Success")
    public void getSuccess() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(1,new Timestamp(1000),"PCR", true, "aaaaaaa");
        doReturn(Optional.of(mockTest)).when(testRepository).findById(1L);

        Optional<fr.polytech.testservice.models.Test> foundTest = testService.get(1L);

        Assertions.assertTrue(foundTest.isPresent(), "Test was not found");
        Assertions.assertSame(foundTest.get(), mockTest, "Tests should be the same");
    }

    @Test
    @DisplayName("get - Not found")
    public void getNotFound() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(1,new Timestamp(1000),"PCR", true, "aaaaaaa");
        doReturn(Optional.empty()).when(testRepository).findById(1L);

        Optional<fr.polytech.testservice.models.Test> foundTest = testService.get(1L);

        Assertions.assertFalse(foundTest.isPresent(), "Test was found when it should not be");
    }

    @Test
    @DisplayName("create - Success")
    public void createSuccess() {
        fr.polytech.testservice.models.Test mockTest = new fr.polytech.testservice.models.Test(1,new Timestamp(1000),"PCR", true, "aaaaaaa");
        doReturn(mockTest).when(testRepository).saveAndFlush(mockTest);

        fr.polytech.testservice.models.Test createdTest = testService.create(mockTest);

        Assertions.assertSame(createdTest, mockTest, "Incoming test and created test should be the same");
    }

    @Test
    @DisplayName("delete - Success")
    public void deleteSuccess() {
        doReturn(true).when(testRepository).existsById(any());
        doNothing().when(testRepository).deleteById(any());
    }

    @Test
    @DisplayName("delete - Failure")
    public void deleteFailure() {
        doThrow(NoSuchElementException.class).when(testRepository).existsById(any());
    }
}
