package fr.polytech.testservice.models;

import javax.persistence.*;
import java.util.Date;

@Entity(name="tests")
@Access(AccessType.FIELD)
public class Test {

    public Test() {}
    
    public Test(Date date, String nom, boolean is_negative, String id_user) {
        this.date = date;
        this.nom = nom;
        this.is_negative = is_negative;
        this.id_user = id_user;
    }

    public Test(long id_test, Date date, String nom, boolean is_negative, String id_user) {
        this.id_test = id_test;
        this.date = date;
        this.nom = nom;
        this.is_negative = is_negative;
        this.id_user = id_user;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id_test;

    private Date date;
    private String nom;
    private boolean is_negative;
    private String id_user;

    public long getId_test() {
        return id_test;
    }

    public void setId_test(long id_test) {
        this.id_test = id_test;
    }

    public boolean isIs_negative() {
        return is_negative;
    }

    public void setIs_negative(boolean is_negative) {
        this.is_negative = is_negative;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String user) {
        this.id_user = user;
    }
}
