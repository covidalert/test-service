package fr.polytech.testservice.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.polytech.testservice.models.Test;

public interface TestRepository extends JpaRepository<Test,Long> { }