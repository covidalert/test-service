package fr.polytech.testservice.controllers;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import fr.polytech.testservice.models.Test;
import fr.polytech.testservice.services.TestService;

@RestController
@RequestMapping("/tests")
@CrossOrigin(origins="*", allowedHeaders = "*")
public class TestsController {
    @Autowired
    private TestService testService;

    @GetMapping("{id}")
    public ResponseEntity<Object> get(@PathVariable Long id) {
        Optional<Test> optionalTest = testService.get(id);
        if(optionalTest.isPresent()) {
            return ResponseEntity.ok(optionalTest.get());
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping()
    public ResponseEntity<Object> post(@RequestBody final Test test) {
        try {
            Test createdTest = testService.create(test);
            return ResponseEntity.created(new URI("/tests/"+createdTest.getId_test())).build();
        } catch(URISyntaxException e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping({"{id}"})
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        try {
            testService.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch(NoSuchElementException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getTestsByUser(@PathVariable String id) {

        List<Test> listTests = testService.getTestsByUser(id);
        return ResponseEntity.ok(listTests);
    }
}
