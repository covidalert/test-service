package fr.polytech.testservice.services;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.polytech.testservice.models.Test;
import fr.polytech.testservice.repositories.TestRepository;

@Service
public class TestService {
    @Autowired
    private TestRepository testRepository;

    public Optional<Test> get(Long id) {
        return testRepository.findById(id);
    }

    public Test create(Test test){
        return testRepository.saveAndFlush(test);
    }

    public void delete(Long id) throws NoSuchElementException {
        if(testRepository.existsById(id)){
            testRepository.deleteById(id);
        } else {
            throw new NoSuchElementException();
        }
    }

    public List<Test> getTestsByUser(String id) {
        List<Test> listUser = new ArrayList<Test>();
        List<Test> listFromBD = testRepository.findAll();
        for(Test test : listFromBD){
            if((test.getId_user()).equals(id)){
                listUser.add(test);
            }
        }
        return listUser;
    }
}
