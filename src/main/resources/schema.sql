CREATE TABLE tests
(
    id_test serial NOT NULL PRIMARY KEY,
    date timestamp NOT NULL,
    nom varchar(64) NOT NULL,
    is_negative bool NOT NULL,
    id_user varchar(36) NOT NULL
);